# dsp-project 项目流程图
![cmd-markdown-logo](http://on-img.com/chart_image/5ad803a3e4b0518eacbe3f83.png?_=1524136689468)

# dsp-project 项目总结

  ## 项目技术选型

    dsp-project项目是作业帮的一个广告发布后台管理系统，使用的是vue技术, 包括vue-cli脚手架、element-ui框架、Echarts、vuex数据管理、vue-router路由库、es6语法、less编译器。
    es6: export和import 的模块加载功能非常方便，适合单文件组件的开发。
    element-ui：基于Vue2.0 作为基础框架实现的组件库，组件丰富，强大的社区支持，面向企业级的后台应用，能够快速的搭建网站，极大地减少研发的人力与时间成本
    less：编译结构清晰，便于扩展，而且可以轻松实现继承。但是在vue-cli脚手架中并没有配置less,所以需
          要下载less、less-loader和node-less模块。

  ## 路由的搭建

    路由信息全部放在router目录下的config.js文件中管理,并使用路由守卫中的钩子函数Router.beforeEach进行了路由拦截操作，在每一次跳转路由时都会判断用户是否是登录状态,如果是登录状态，则继续next到下一个路由，若是非法操作则会跳转到登录注册页面，确保只有在登录状态下才能访问后台管理系统，提高系统操作的安全性。

  ## 核心功能 

    本项目的核心功能就是广告发布与管理：包括广告创意、广告计划 与 广告单元三个部分，在广告创意部分，用户可以将本地文件（包括图片）上传，并添加广告的文案描述与监控链接；同时也可以在广告计划部分建立新的广告计划，包括推广目标、计划名称以及每日投放的资金限额；用户还可以在广告单元部分设置单元的基本信息、投放周期、覆盖定向（包括性别、年龄、地域等）、广告位、出价等；以上操作均可在操作完成后根据内容生成列表，并可以在列表页进行增、删、改、查的操作

  ## 问题与解决方法

    在新建创意上传图片时，会根据上传的创意生成一个tab切换的组件，在element-ui库中并未找到合适的组件,此时就需要自行封装一个tab切换。在组件封装完成后点击tab切换时，出现了一个bug:内容展示并没有做相应的切换，只是展示第一个创意的内容，此时就需要将内容展示部分拆分出来单独封装成一个组件，根据父组件传递的变量来展示对应的内容。

  ## 数据管理  

    在登录注册页面使用了vuex来管理数据，将用户的登录信息以及token存入全局数据store中，在首页中通过mapState获取到用户信息，更新首页中的用户信息展示；由于store中的数据是存储在缓存中的，刷新浏览器后数据就会消失，为避免这一问题需要将生成的token字段存储在localStorage中一份，用做判断用户是否登录的一种依据。

  ## 打包上线

    npm run build 打包上线，如果打包后运行是空白的话，需要在config的index.js中配置两个选项：
    assetsPublicPath: './'   修改静态文件的路径，打包后静态文件就在当期目录下，所以修改为'./'  productionSourceMap: false   环境设置为生产环境





For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
